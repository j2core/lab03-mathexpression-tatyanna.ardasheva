import java.util.ArrayList;
import java.util.Scanner;
import java.math.*;
import java.lang.String;

public class MathExprToString {

    public static final char DIV = '/';
    public static final char MULTY = '*';
    public static final char DIFF = '-';
    public static final char SUM = '+';
    public static final char POINT = '.';


    public static void main(String[] args) {
        String str = inputMathExpressionFromConsole();
        checkEmptyLine(str);



        ArrayList<Double> listOfParseNumbersMathExpr = new ArrayList<Double>();
        ArrayList<Character> listOfMathExprSig = new ArrayList<Character>();


    }


    private static String inputMathExpressionFromConsole (){
        System.out.println("Please, enter simple math expression \n For example 3/4*5+6-7/2-1*4/1.5:");
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        return str;
    }

    private static void checkEmptyLine(String str){
        if (str.length() == 0){
            System.out.println("Error. Do not enter anyting. \nProgram exit.");
            System.exit(1);
        }
    }

    private static String replaceCommaAndSpace(String str){
        return str.replaceAll(",", ".").replaceAll(" ","");
    }

    private static String parserExpression(String str, ArrayList<Double> listOfParseNumbersMathExpr, ArrayList<Character> listOfMathExprSig )
            throws Exception {

        int i = 0;
        int tmp = 0;
        boolean negative = false;
        boolean positive = true;

        if (str.charAt(i) == DIV || str.charAt(i) == MULTY){
            i++;

            if ((str.charAt(i) == DIV ||str.charAt(i) == MULTY) && (str.charAt(i+1) == DIV || str.charAt(i+1) == MULTY)){
                System.out.println("You have entered an incorrect expression that contains excess mathematical symbols. '\"+str+\"'");
                System.exit(1);
            }
        }

        if (str.charAt(i) == DIFF){
            negative = true;
            str = str.substring(1);
        }

        if (str.charAt(i) == SUM){
            positive = true;
            str = str.substring(1);
        }

        while (i<str.length()){
            int j =i;

            if (i == 0){
                System.out.println("You have entered an incorrect expression that contains excess mathematical symbols. '"+str+"'");
                throw new Exception("You have entered an incorrect expression that contains excess mathematical symbols.");
            }

            if (i<str.length() && Character.isDigit(str.charAt(i)) || str.charAt(i) == POINT) {

                if (str.charAt(i) == POINT && ++tmp > 1) {
                    System.out.println("You enter incorrect number containing two or more points '" + str.substring(0, i + 1) + "'");
                    throw new Exception("You enter incorrect number containing two or more points");
                }
                i++;
            }

            double data = Double.parseDouble(str.substring(j, i));
            if (negative) {
                data = -data;
                negative = false;
            }

            listOfParseNumbersMathExpr.add(data);

            if (i<str.length()){
                char operation = str.charAt(i);
                listOfMathExprSig.add(operation);
            }

            if (i<str.length()){

                if(DIFF == str.charAt(i)){
                    negative = true;
                    i++;
                }

                if(SUM == str.charAt(i)){
                    positive = true;
                    i++;

                }

            }
        }


        return parserExpression(str, listOfParseNumbersMathExpr,listOfMathExprSig );


    }

    private static ArrayList <Double> parserToNumber(String str){

        ArrayList<Double> myListOfParserNumbers =new ArrayList<Double>();


        return myListOfParserNumbers;
    }

}

