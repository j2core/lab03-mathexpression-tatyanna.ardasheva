package com.j2core.ard.week03;

import java.util.ArrayList;
import java.lang.Character;
import static com.j2core.ard.week03.MyCalc.calcNum;


public class ParserString {

    public static final char DIV = '/';
    public static final char MULTI = '*';
    public static final char SUMM = '+';
    public static final char DIFF = '-';
    public static final char POINT = '.';


    private static String replaceMathOperations(String str) {
        return str.replaceAll(" ", "").replaceAll("([+]-|-[+])", "-").
                replaceAll("([+]+|[-]-)", "+").replaceAll("([.]data1)", "(data1*0.1)").
                replaceAll("([.]data2)", "(data2*0.1)").replaceAll("(,)", ".");
        }

    static boolean isCharacter(char c) {
        return c == SUMM || c == DIFF || c == MULTI || c == DIV;
        }

        

    private static String checkString(String str) {
        StringBuilder chSt = new StringBuilder();
        String operations = replaceMathOperations(str);

        for (int i = 0; i < operations.length(); i++) {
            if (operations.charAt(i) == DIV || operations.charAt(i) == MULTI || operations.charAt(i) == SUMM ||
                    operations.charAt(i) == DIFF || operations.charAt(i) == POINT) {
                if (isCharacter(operations.charAt(i + 1)) && operations.charAt(i) != operations.charAt(i + 1)) {
                    System.out.println(" Error! Incorrect input \n Program exit.");
                    System.exit(1);
                }
                if (i < operations.length() && (operations.charAt(i) == operations.charAt(i + 1))) {
                    i++;
                }

                chSt.append(operations.charAt(i));

            } else {
                chSt.append(operations.charAt(i));
            }


        }

        return chSt.toString();
    }


    public static void mathSolver(String str) {

        double ms;
        int i;
        String strok = checkString(str);
        if (str.length() > 0) {

            ArrayList<Double> listOfNumbersMathExpr = parseToNumber(strok);
            ArrayList<Character> listOfMathExprSig = parseToArrayMathSign(strok);

            for ( i = 0; i < listOfMathExprSig.size(); i++) {
                if ( listOfMathExprSig.get(i) == DIV || listOfMathExprSig.get(i) == MULTI ){
                    ms = calcNum(listOfNumbersMathExpr.get(i), listOfNumbersMathExpr.get(i + 1), listOfMathExprSig.get(i));
                    listOfNumbersMathExpr.set(i, ms);
                    listOfNumbersMathExpr.remove(i + 1);
                    listOfMathExprSig.remove(i);
                    i--;
                }
            }

            for ( i = 0; i < listOfMathExprSig.size(); i++) {
                if (listOfMathExprSig.get(i) == SUMM || listOfMathExprSig.get(i) == DIFF) {
                    ms = calcNum(listOfNumbersMathExpr.get(i), listOfNumbersMathExpr.get(i + 1), listOfMathExprSig.get(i));
                    listOfNumbersMathExpr.set(i, ms);
                    listOfNumbersMathExpr.remove(i + 1);
                    listOfMathExprSig.remove(i);
                    i--;
                }
            }

            System.out.println("Answer: " + listOfNumbersMathExpr.get(i));
        }
    }



    private static ArrayList<Double> parseToNumber(String str) {
        ArrayList<Double> listOfParseNumbersMathExpr = new ArrayList<Double>();



        int i = 0;
        boolean negative = false;
        boolean positive = true;

        if( str.charAt(i) == DIFF ) {
            negative = true;
            str = str.substring(1);
        }
        if( str.charAt(i) == SUMM ){
            positive = true;
            str = str.substring( 1 );
        }

        for (i = 0; i < str.length(); i++) {
            int j = i;

          /*  if (str.charAt(i) == DIFF && Character.isDigit(str.charAt(i+1)) && i == 0){
                i++;
            }

            if (str.charAt(i) == SUMM && Character.isDigit(str.charAt(i+1)) && i == 0){
                i++;
            }
*/

            while (i < str.length() && (Character.isDigit(str.charAt(i)) || str.charAt(i) == POINT)) {
                i++;
            }

            double data = Double.parseDouble(str.substring(j, i));
            if (negative) {
                data = -data;
                negative = false;
            }

            listOfParseNumbersMathExpr.add(data);



           // listOfParseNumbersMathExpr.add(Double.parseDouble(str.substring(j, i)));
        }

        return listOfParseNumbersMathExpr;
    }


    private static ArrayList<Character> parseToArrayMathSign(String str) {

        ArrayList<Character> listOfMathOperators = new ArrayList<Character>();
        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if ((str.charAt(i) == DIFF || str.charAt(i) == SUMM) && i == 0) {
                i++;
            }

            if (isCharacter(str.charAt(i))) {
            listOfMathOperators.add(str.charAt(i));
            }
        }

        return listOfMathOperators;
    }
}