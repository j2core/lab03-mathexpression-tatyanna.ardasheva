package com.j2core.ard.week03;

/* created by Tatiana Ardasheva
    * starting 21/09/18 and end of work is not defined;
    *
    * Calculation math expression 3/4*5+6-7/2-1*4/1.5 with expected result 3.58;
    *
 */
import java.util.Scanner;
import java.lang.String;
import static com.j2core.ard.week03.ParserString.mathSolver;

public class MyMathExpression {


    public static void main(String[] args) {
        String str = inputMathexpressionFromConsole();
        printExpression(str);
        mathSolver(str);
    }

    private static String inputMathexpressionFromConsole(){

        System.out.println("Please, enter simple math expression, \nfor example: 3/4*5+6-7/2-1*4/1.5");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();

        return str;
    }

    private static void printExpression (String str) {

        if (str.length() == 0) {
            System.out.println("Error. Do not enter anyting. \nProgram exit.");
            System.exit(1);
        }

    }

}

