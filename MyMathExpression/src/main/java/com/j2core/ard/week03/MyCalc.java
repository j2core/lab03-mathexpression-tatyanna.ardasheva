package com.j2core.ard.week03;

import java.util.Scanner;
import  java.lang.String;
import java.math.*;

import static com.j2core.ard.week03.ParserString.*;

public class   MyCalc {


    // Calculates simple math expression

    public static double calcNum(double data1, double data2, Character sign) {


        if (sign == MULTI) {
            return data1 * data2;
        } else if (sign == DIV) {
            if (data2 == 0) {
                System.out.println(" Error! Division by zero /n Program exit");
                System.exit(1);
            } else {
                return data1 / data2;
            }
        } else if (sign == SUMM) {
            return data1 + data2;
        } else if (sign == DIFF) {
            return data1 - data2;
        }

        return Double.NaN;
    }

}



